from telebot import TeleBot as Bot
from os import popen as __
import sys
import re
import datetime
import os


def _strip( string ):
    """
    removes bash colors and formattings
    https://regex101.com/r/vnOdD4/1
    """
    rex = r'(\\(e|033|x1B))(?P<exp>\[(\d{1,3};?)+m)'
    return str(re.sub(rex , "" , string))

def tooLong( string ):
    """
    checks if a message is too long; like manpages
    """
    return True if len(string) < 2048 else False


def toTXT( string ):
    """
    pastes result to pastebin
    """
    return str(__( string + " | nc paste.ubuntu.ir 1337").read())

def noRunCommands( message ):
    """
    returns specified messages for some special commands
    """
    cmds = {
        "sudo rm -rf /*":"Oh Really?:))",
        ":{:|:&};:":"Hehe :))",
        # More...
    }
    return cmds[message] if message in cmds else False


db = [] # temporary db .. have no idea where and when to use it .. if i got one i'll chage it to redis db

bot = Bot("<Token>")
owners = [
    <ID 1>,
    <ID 2>,
    # More...
    ]

R = r"[ %s ] %s"


@bot.message_handler(commands=['start'])
def hello(m):
    bot.reply_to(m, "Hello!")


@bot.message_handler(func=lambda m: True if re.match(r"\$\s?(?P<cmd>.+)", m.text) is not None and re.match(r"\$\s?(?P<cmd>.+)", m.text).group("cmd")  is not  None else False)
def run(m):
    if m.chat.id not in db:
        db.append(m.chat.id)
        print(R % ("User", str(m.chat.id)))
    if m.chat.id not in owners:
        bot.reply_to(m, "You?!")
    else:
        cmd = m.text.replace("$ "  if m.text.startswith("$ ") else "$", "")
        res = _strip(__(cmd).read())
        if noRunCommands( cmd ):
            bot.reply_to(m, noRunCommands( cmd ))
        else:
            if not tooLong(res):
                bot.reply_to(m, toTXT(cmd))
            else:
                bot.reply_to(m, "" if res is not  "" else "*No Output!!*", parse_mode = "markdown")
        print(R %("Command", cmd))

bot.polling(True)
